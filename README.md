# Clojure - CodeKata

### This is an attempt with Clojure at solving [CodeKata problems](http://codekata.com/)

### Currently solved:

*  [Kata09: Back to the Checkout](http://codekata.com/kata/kata09-back-to-the-checkout/)

## License

Copyright © 2021 Prashant Sinha

Distributed under the Eclipse Public License version 1.0.