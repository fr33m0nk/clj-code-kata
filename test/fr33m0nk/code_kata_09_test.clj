(ns fr33m0nk.code-kata-09-test
  (:require [clojure.test :refer :all]
            [fr33m0nk.code-kata-09 :refer :all]))

(def pricing-policy {:A {:price 50
                         :offer {:quantity 3
                                 :price    130}}
                     :B {:price 30
                         :offer {:quantity 2
                                 :price    45}}
                     :C {:price 20}
                     :D {:price 15}})

(deftest load-config-test
  (testing "should load pricing-policy when file name is provided"
    (is (= pricing-policy (load-config "pricing_config.edn")))))

(deftest add-to-cart-test
  (testing "should add consumerables to to the cart under key :items"
    (let [cart (atom [])]
      (are [x y] (= x (add-to-cart cart y))
                 [:A] :A
                 [:A :B] :B
                 [:A :B :A] :A))))

(deftest get-pricing-policy-for-item-test
  (testing "should return a map with price, offer quantity and offer price"
    (is (= {:offer-price    130
            :offer-quantity 3
            :price          50} (get-pricing-policy-for-item pricing-policy :A))))
  (testing "should return a map with price, offer quantity and offer price with default 0 , nil and 0 respectively"
    (is (= {:offer-price    0
            :offer-quantity nil
            :price          0} (get-pricing-policy-for-item pricing-policy :E)))))

(deftest get-combo-and-leftover-items-tuple-test
  (testing "should return a tuple with combination and leftover items"
    (are [x y z] (= x (get-combo-and-leftover-items-tuple y z))
                 [0 5] 6 5
                 [1 2] 3 5)))

(deftest calculate-total-recursive-test
  (testing "should calculate the price of the item for given quantity using pricing rules without offer"
    (is (= 150 (calculate-total-recursive pricing-policy [:C :D :C :D :B :A]))))
  (testing "should calculate the price of the item for given quantity using pricing rules with offer"
    (is (= 310 (calculate-total-recursive pricing-policy [:A :B :A :A :B :B :C :A :C :D])))))

(deftest bill-as-items-are-added-to-cart-recursive-test
  (testing "should calculate the price of an empty cart as 0"
    (is (= 0 (bill-as-items-are-added-to-cart-recursive (atom [])))))
  (testing "should calculate the price of the cart as items are added"
    (let [cart (atom [])]
      (are [x y] (= x (bill-as-items-are-added-to-cart-recursive cart y))
                 50 :A
                 80 :B
                 130 :A
                 160 :A
                 175 :B))))

(deftest calculate-total-test
  (testing "should calculate the price of the item for given quantity using pricing rules without offer"
    (is (= 150 (calculate-total pricing-policy [:C :D :C :D :B :A]))))
  (testing "should calculate the price of the item for given quantity using pricing rules with offer"
    (is (= 310 (calculate-total pricing-policy [:A :B :A :A :B :B :C :A :C :D])))))

(deftest bill-as-items-are-added-to-cart-test
  (testing "should calculate the price of an empty cart as 0"
    (is (= 0 (bill-as-items-are-added-to-cart (atom [])))))
  (testing "should calculate the price of the cart as items are added"
    (let [cart (atom [])]
      (are [x y] (= x (bill-as-items-are-added-to-cart cart y))
                 50 :A
                 80 :B
                 130 :A
                 160 :A
                 175 :B))))
