(ns fr33m0nk.code-kata-09
  (:gen-class)
  (:require
    [clojure.java.io :as io]
    [clojure.edn :as edn]))

(defn load-config
  [file-name]
  (-> file-name io/resource slurp edn/read-string))

(defn get-pricing-policy-for-item
  [pricing-policy item]
  {:price          (get-in pricing-policy [item :price] 0)
   :offer-quantity (get-in pricing-policy [item :offer :quantity])
   :offer-price    (get-in pricing-policy [item :offer :price] 0)})

(defn add-to-cart
  [cart item]
  (swap! cart conj item))

(defn get-combo-and-leftover-items-tuple
  [offer-quantity quantity-in-cart]
  (if offer-quantity
    [(quot quantity-in-cart offer-quantity) (rem quantity-in-cart offer-quantity)]
    [0 quantity-in-cart]))

(defn calculate-total-recursive
  ([pricing-policy items]
   (let [grouped-cart-items (group-by identity items)]
     (calculate-total-recursive pricing-policy grouped-cart-items 0)))
  ([pricing-policy [[name items] & rest] amount]
   (let [quantity-in-cart (count items)
         {:keys [price offer-quantity offer-price]} (get-pricing-policy-for-item pricing-policy name)
         [offer-combos remaining-items] (get-combo-and-leftover-items-tuple offer-quantity quantity-in-cart)]
     (if (nil? name)
       amount
       (recur pricing-policy rest
              (+ amount
                 (* price remaining-items)
                 (* offer-price offer-combos)))))))

(defn bill-as-items-are-added-to-cart-recursive
  "cart: an atom with [] where items can be added
  items: rest argument, can have single as well as many items "
  [cart & items]
  (let [pricing-policy (load-config "pricing_config.edn")]
    (doseq [i items]
      (add-to-cart cart i))
    (calculate-total-recursive pricing-policy @cart)))

(defn calculate-total
  [pricing-policy cart-items]
  (->> cart-items
       (group-by identity)
       (map (fn [[name items]]
              (let [quantity-in-cart (count items)
                    {:keys [price offer-quantity offer-price]} (get-pricing-policy-for-item pricing-policy name)
                    [offer-combos remaining-items] (get-combo-and-leftover-items-tuple offer-quantity quantity-in-cart)]
                (+ (* offer-combos offer-price)
                   (* remaining-items price)))))
       (apply +)))

(defn bill-as-items-are-added-to-cart
  "cart: an atom with [] where items can be added
  items: rest argument, can have single as well as many items "
  [cart & items]
  (let [pricing-policy (load-config "pricing_config.edn")]
    (doseq [i items]
      (add-to-cart cart i))
    (calculate-total pricing-policy @cart)))


